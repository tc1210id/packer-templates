source "qemu" "rockylinux-8" {
  accelerator       = "kvm"
  boot_command      = [ "<esc><wait>linux inst.ks=http://{{.HTTPIP}}:{{.HTTPPort}}/ks.cfg<enter>"]
  boot_wait         = "10s"
  disk_interface    = "virtio"
  disk_size         = "32768M"
  format            = "qcow2"
  headless          = true
  http_directory    = "http"
  iso_checksum      = "sha256:06019fd7c4f956b2b0ed37393e81c577885e4ebd518add249769846711a09dc4"
  iso_url           = "https://download.rockylinux.org/pub/rocky/8/isos/x86_64/Rocky-8.9-x86_64-minimal.iso"
  machine_type      = "q35"
  memory            = 2048
  net_device        = "virtio-net"
  output_directory  = "output-8"
  shutdown_command  = "echo 'packer' | sudo -S shutdown -P now"
  ssh_password      = "vagrant"
  ssh_timeout       = "20m"
  ssh_username      = "vagrant"
  vm_name           = "rl-8"
}

build {
  sources = ["source.qemu.rockylinux-8"]

  provisioner "shell" { 
    inline = [ 
      "sudo dnf update -y",
      "sudo dnf clean all"
    ]
  }

  post-processor "vagrant" {
    output          = "rockylinux-8-{{isotime `20060102`}}-x86_64.{{.Provider}}.box"
  }
}

