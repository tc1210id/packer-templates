source "qemu" "rockylinux-9" {
  accelerator       = "kvm"
  boot_command      = [ "<esc><wait>linux inst.ks=http://{{.HTTPIP}}:{{.HTTPPort}}/ks.cfg<enter>"]
  boot_wait         = "10s"
  disk_interface    = "virtio"
  disk_size         = "32768M"
  format            = "qcow2"
  headless          = true
  http_directory    = "http"
  iso_checksum      = "sha256:eef8d26018f4fcc0dc101c468f65cbf588f2184900c556f243802e9698e56729"
  iso_url           = "https://download.rockylinux.org/pub/rocky/9/isos/x86_64/Rocky-9.3-x86_64-minimal.iso"
  machine_type      = "q35"
  memory            = 2048
  net_device        = "virtio-net"
  output_directory  = "output-9"
  shutdown_command  = "echo 'packer' | sudo -S shutdown -P now"
  ssh_password      = "vagrant"
  ssh_timeout       = "20m"
  ssh_username      = "vagrant"
  vm_name           = "rl-9"
  qemuargs          = [ [ "-cpu", "host" ] ] ## See issue for information https://github.com/hashicorp/packer-plugin-qemu/issues/76
}

build {
  sources = ["source.qemu.rockylinux-9"]

  provisioner "shell" { 
    inline = [ 
      "sudo dnf update -y",
      "sudo dnf clean all"
    ]
  }

  post-processor "vagrant" {
    output          = "rockylinux-9-{{isotime `20060102`}}-x86_64.{{.Provider}}.box"
  }
}

