source "qemu" "almalinux-8" {
  accelerator       = "kvm"
  boot_command      = [ "<esc><wait>linux inst.ks=http://{{.HTTPIP}}:{{.HTTPPort}}/ks.cfg<enter>"]
  boot_wait         = "10s"
  disk_interface    = "virtio"
  disk_size         = "32768M"
  format            = "qcow2"
  headless          = true
  http_directory    = "http"
  iso_checksum      = "sha256:fc866e1280e7b3f066b1380e831e33e7f3e78bc9db7bfc27744d569eadd974b3"
  iso_url           = "https://repo.almalinux.org/almalinux/8.9/isos/x86_64/AlmaLinux-8.9-x86_64-minimal.iso"
  machine_type      = "q35"
  memory            = 2048
  net_device        = "virtio-net"
  output_directory  = "output-8"
  shutdown_command  = "echo 'packer' | sudo -S shutdown -P now"
  ssh_password      = "vagrant"
  ssh_timeout       = "20m"
  ssh_username      = "vagrant"
  vm_name           = "rl-8"
}

build {
  sources = ["source.qemu.almalinux-8"]

  provisioner "shell" { 
    inline = [ 
      "sudo dnf update -y",
      "sudo dnf clean all"
    ]
  }

  post-processor "vagrant" {
    output          = "almalinux-8-{{isotime `20060102`}}-x86_64.{{.Provider}}.box"
  }
}

