source "qemu" "almalinux-9" {
  accelerator       = "kvm"
  boot_command      = [ "<esc><wait>linux inst.ks=http://{{.HTTPIP}}:{{.HTTPPort}}/ks.cfg<enter>"]
  boot_wait         = "10s"
  disk_interface    = "virtio"
  disk_size         = "32768M"
  format            = "qcow2"
  headless          = true
  http_directory    = "http"
  iso_checksum      = "sha256:6624593b53c89195f7b68b2070a280d47b4276a7cbc10d2216661bf35d4f442b"
  iso_url           = "https://repo.almalinux.org/almalinux/9.3/isos/x86_64/AlmaLinux-9.3-x86_64-minimal.iso"
  machine_type      = "q35"
  memory            = 2048
  net_device        = "virtio-net"
  output_directory  = "output-9"
  shutdown_command  = "echo 'packer' | sudo -S shutdown -P now"
  ssh_password      = "vagrant"
  ssh_timeout       = "20m"
  ssh_username      = "vagrant"
  vm_name           = "rl-9"
  qemuargs          = [ [ "-cpu", "host" ] ] ## See issue for information https://github.com/hashicorp/packer-plugin-qemu/issues/76
}

build {
  sources = ["source.qemu.almalinux-9"]

  provisioner "shell" { 
    inline = [ 
      "sudo dnf update -y",
      "sudo dnf clean all"
    ]
  }

  post-processor "vagrant" {
    output          = "almalinux-9-{{isotime `20060102`}}-x86_64.{{.Provider}}.box"
  }
}

