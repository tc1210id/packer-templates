source "proxmox-iso" "almalinux-9" {
  # Promox Access Configuration
  proxmox_url = "${var.proxmox_api_url}"
  # username = "${var.proxmox_api_token_id}"
  # token = "${var.proxmox_api_token_secret}"
  username = "${var.proxmox_username}"
  password = "${var.proxmox_password}"
  insecure_skip_tls_verify = true

  # VM General Settings
  node = "pm-01-20230502"
  vm_id = "1005"
  vm_name = "almalinux9-server"
  template_description = "Alma Linux 9 Server Template generated on {{ isotime \"2006-01-02T15:04:05Z\" }}"


  # Base ISO File Configuration
  iso_file = "truenas_iso_images:iso/AlmaLinux-9.3-x86_64-minimal.iso"
  iso_storage_pool = "truenas_iso_images"
  unmount_iso = true

  # OS Settings
  os = "l26"

  # Entropy Setting
  rng0 {
    source = "/dev/urandom"
    max_bytes = 1024
    period = 1000
  }

  # VM System Settings
  qemu_agent = true

  # VM Hard Disk Settings
  scsi_controller = "virtio-scsi-pci"

  # Storage
  disks {
    disk_size = "32G"
    format = "qcow2"
    storage_pool = "truenas_proxmox"
    type = "virtio"
  }

  # VM CPU Settings
  cores = "2"
  cpu_type = "x86-64-v2-AES"

  # VM Memory Settings
  memory = "2048"

  # VM Machine Type
  machine = "q35"

  # VM Network Settings
  network_adapters {
    model = "virtio"
    bridge = "vmbr0"
    firewall = "false"
  }

  # Packer Boot Commands
  boot_command = [ "<esc><wait>linux inst.ks=http://{{.HTTPIP}}:{{.HTTPPort}}/ks.cfg<enter>"]
  boot_wait = "5s"

  # Packer Autoinstall Settings
  http_directory = "http"

  ssh_username = "vagrant"
  ssh_password = "vagrant"

  ssh_timeout = "20m"
}

build {

  name = "almalinux-9"
  sources = ["source.proxmox-iso.almalinux-9"]

  provisioner "shell" { 
    inline = [ 
      "sudo dnf update -y",
      "sudo dnf clean all"
    ]
  }
}
