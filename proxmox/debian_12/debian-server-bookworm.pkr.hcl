# Debian Server Bookworm
# ---
# Packer Template to create an Debian Server (bookworm) on Proxmox
#
# Variable Definitions
variable "proxmox_api_url" {
    type = string
}

variable "proxmox_api_token_id" {
    type = string
}

variable "proxmox_api_token_secret" {
    type = string
    sensitive = true
}

variable "proxmox_username" {
    type = string
}

variable "proxmox_password" {
    type = string
    sensitive = true
}


# Resource Definiation for the VM Template
source "proxmox-iso" "debian-server-bookworm" {
 
    # Proxmox Connection Settings
    proxmox_url = "${var.proxmox_api_url}"
    # username = "${var.proxmox_api_token_id}"
    # token = "${var.proxmox_api_token_secret}"
    username = "${var.proxmox_username}"
    password = "${var.proxmox_password}"
    insecure_skip_tls_verify = true
    
    # VM General Settings
    node = "pm-01-20230502" # add your proxmox node
    vm_id = "1000"
    vm_name = "debian-server-bookworm"
    template_description = "Debian Server Bookworm Image, generated ${timestamp()}"

    # VM OS Settings
    # (Option 1) Local ISO File - Download Ubuntu ISO and Upload To Proxmox Server
    iso_file = "truenas_iso_images:iso/debian-12.2.0-amd64-netinst.iso"
    iso_storage_pool = "truenas_iso_images"
    unmount_iso = true

    # OS Settings
    os = "l26"

    # VM System Settings
    qemu_agent = true

    # Entropy Setting
    rng0 {
      source = "/dev/urandom"
      max_bytes = 1024
      period = 1000
    }

    # VM Hard Disk Settings
    scsi_controller = "virtio-scsi-pci"

    disks {
        disk_size = "32G"
        format = "qcow2"
        storage_pool = "truenas_proxmox"
        type = "virtio"
    }

    # VM CPU Settings
    cores = "2"
    cpu_type = "x86-64-v2-AES"
    
    # VM Memory Settings
    memory = "2048" 

    # VM Machine type
    machine = "q35"

    # VM Network Settings
    network_adapters {
        model = "virtio"
        bridge = "vmbr0"
        firewall = "false"
    } 

    # PACKER Boot Commands
    boot_command = ["<esc><wait><esc><wait>auto preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<enter>"]
    boot_wait = "5s"

    # PACKER Autoinstall Settings
    http_directory = "http" 
    # (Optional) Bind IP Address and Port
    # http_bind_address = "0.0.0.0"
    # http_port_min = 8802
    # http_port_max = 8802

    ssh_username = "vagrant"
    ssh_password = "vagrant"

    # (Option 1) Add your Password here
    # ssh_password = "your-password"
    # - or -
    # (Option 2) Add your Private SSH KEY file here
    # ssh_private_key_file = "~/.ssh/id_rsa"

    # Raise the timeout, when installation takes longer
    ssh_timeout = "20m"
}

# Build Definition to create the VM Template
build {

    name = "debian-server-bookworm"
    sources = ["source.proxmox-iso.debian-server-bookworm"]

    provisioner "shell" {
        inline = [
            "sudo apt -y autoremove --purge",
            "sudo apt -y clean",
            "sudo apt -y autoclean",
            "sudo sync"
        ]
    }

}
