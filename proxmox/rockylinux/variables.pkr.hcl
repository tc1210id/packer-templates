variable "proxmox_api_url" {
  type = string
  default = "https://pm-01-20230502.home.local:8006/api2/json"
}

variable "proxmox_api_token_id" {
  type = string
  default = "root@localhost"
}

variable "proxmox_api_token_secret" {
  type = string
  sensitive = true
  default = "password"
}

variable "proxmox_username" {
  type = string
  default = "root"
}

variable "proxmox_password" {
  type = string
  sensitive = true
  default = "password"
}
